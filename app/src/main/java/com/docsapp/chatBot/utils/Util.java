package com.docsapp.chatBot.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Ashish on 06-05-2018.
 */

public class Util {
    public final static String BASE_URL = "https://www.personalityforge.com/api/chat/?apiKey=6nt5d1nJHkqbkphe&message=%s&chatBotID=63906&externalID=chirag1";
    public final static String API_KEY = "nt5d1nJHkqbkphe";

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
