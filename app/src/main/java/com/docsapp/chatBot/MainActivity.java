package com.docsapp.chatBot;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.docsapp.chatBot.adapter.MessageAdapter;
import com.docsapp.chatBot.model.ChatResponse;
import com.docsapp.chatBot.model.MessageModel;
import com.docsapp.chatBot.network.NetworkManager;
import com.docsapp.chatBot.network.ResponseListener;
import com.docsapp.chatBot.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mMessageRecycler;
    private MessageAdapter mMessageAdapter;
    private EditText mChatInputText;
    List<MessageModel> mMessageList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NetworkManager.init(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mChatInputText = findViewById(R.id.edittext_chatbox);
        findViewById(R.id.button_chatbox_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String content = mChatInputText.getText().toString();
                if (!TextUtils.isEmpty(content)) {
                    MessageModel messageModel = new MessageModel();
                    messageModel.setMessage(content);
                    messageModel.setType(1);
                    mMessageList.add(messageModel);
                    mMessageAdapter.notifyDataSetChanged();
                    scrollList();
                    mChatInputText.setText("");
                    if (Util.isNetworkAvailable(MainActivity.this)) {
                        NetworkManager.apiCall(MainActivity.this, content, new ResponseListener() {
                            @Override
                            public void OnSuccessResponse(ChatResponse response) {
                                if (!TextUtils.isEmpty(response.getMessage().getMessage())) {
                                    MessageModel messageModel = new MessageModel();
                                    messageModel.setMessage(response.getMessage().getMessage());
                                    messageModel.setType(2);
                                    mMessageList.add(messageModel);
                                    mMessageAdapter.notifyDataSetChanged();
                                    scrollList();
                                }
                            }

                            @Override
                            public void onFailedResponse() {
                                Toast.makeText(MainActivity.this, "Error...", Toast.LENGTH_SHORT).show();
                            }

                        });
                    } else {
                        Toast.makeText(MainActivity.this, "Please connect to network ..", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        mMessageRecycler = findViewById(R.id.reyclerview_message_list);
        mMessageRecycler.setNestedScrollingEnabled(false);
        mMessageAdapter = new MessageAdapter(this, mMessageList);
        mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));
        mMessageRecycler.setAdapter(mMessageAdapter);

        mMessageRecycler.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v,
                                       int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    scrollList();
                }
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void scrollList(){
        mMessageRecycler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mMessageRecycler.getAdapter().getItemCount() > 1) {
                    mMessageRecycler.smoothScrollToPosition(
                            mMessageRecycler.getAdapter().getItemCount() - 1);
                }
            }
        }, 100);
    }
}
