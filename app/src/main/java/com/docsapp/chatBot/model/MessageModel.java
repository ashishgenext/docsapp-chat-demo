package com.docsapp.chatBot.model;

/**
 * Created by Ashish on 06-05-2018.
 */

public class MessageModel {
    private String message ;
    private String timeStamp ;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    private int type;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
