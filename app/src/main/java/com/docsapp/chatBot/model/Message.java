
package com.docsapp.chatBot.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Message {

    @SerializedName("chatBotID")
    private Long mChatBotID;
    @SerializedName("chatBotName")
    private String mChatBotName;
    @SerializedName("emotion")
    private String mEmotion;
    @SerializedName("message")
    private String mMessage;

    public Long getChatBotID() {
        return mChatBotID;
    }

    public void setChatBotID(Long chatBotID) {
        mChatBotID = chatBotID;
    }

    public String getChatBotName() {
        return mChatBotName;
    }

    public void setChatBotName(String chatBotName) {
        mChatBotName = chatBotName;
    }

    public String getEmotion() {
        return mEmotion;
    }

    public void setEmotion(String emotion) {
        mEmotion = emotion;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}
