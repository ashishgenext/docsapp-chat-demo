package com.docsapp.chatBot.network;

import com.docsapp.chatBot.model.ChatResponse;

/**
 * Created by Ashish on 06-05-2018.
 */

public interface ResponseListener {
     void OnSuccessResponse(ChatResponse response);
     void onFailedResponse();
}
