package com.docsapp.chatBot.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.docsapp.chatBot.model.ChatResponse;
import com.docsapp.chatBot.utils.Util;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Ashish on 06-05-2018.
 */

public class NetworkManager {

    private static RequestQueue mQueue ;

    public static void init(Context context){
        mQueue = Volley.newRequestQueue(context);
    }

    public static void apiCall(Context context, String query , final ResponseListener listener) {
        if(mQueue == null) init(context);

        String url = String.format(Util.BASE_URL, query);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();
                        ChatResponse chatResponse =  gson.fromJson(response.toString(),ChatResponse.class);
                        listener.OnSuccessResponse(chatResponse);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onFailedResponse();
                    }
                }
        );

        mQueue.add(getRequest);
    }


}
