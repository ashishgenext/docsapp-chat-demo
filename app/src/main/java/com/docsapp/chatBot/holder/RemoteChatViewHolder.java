package com.docsapp.chatBot.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.docsapp.chatBot.R;

import com.docsapp.chatBot.model.MessageModel;

/**
 * Created by Ashish on 06-05-2018.
 */

public class RemoteChatViewHolder extends RecyclerView.ViewHolder {
    TextView messageText;

    public RemoteChatViewHolder(View itemView) {
        super(itemView);
        messageText = (TextView) itemView.findViewById(R.id.text_message_body);
    }

    public void bind(MessageModel message) {
        messageText.setText(message.getMessage());
    }
}
